// ТЕОРЕТИЧНІ ПИТАННЯ
/* 1. Як можна оголосити змінну у Javascript?
Змінні можна оголосити за допомогою let, const та var.
let - це змінна, яку можна переприсвоювати і її видимість в області блоку.
const -  це константа, тобто цю змінну не можна переприсвоювати, її видимість також в області блоку.
var - це застаріла змінна і зараз використовується тільки в старих проектах, тому що її область видимості не обмежується блоком і вспливає
 в глобальну видимість, що може призвести до помилок в проекті, які важко відслідковувати.

 2. Що таке рядок (string) і як його створити (перерахуйте всі можливі варіанти)?
 Рядок - це послідовність символів від 0 і більше, літерал якого починається і закінчується одинарними або подвійними лапками.
 Створити рядок можна за допомогою: 
 а) оголошення змінної: let string = "string";
 б) додавання двох або більше рядків: let string = "Hello " + "World" + "!"; в результаті - "Hello World!"
 в) додавання рядка з будь-яким значення: let string = "11" + 10; в результаті - "1110"
 г) приведенням до типу рядок: let string = String(100); в результаті - "100"

 3. Як перевірити тип даних змінної в JavaScript?
 Вивести тип даних можна вивевши в конслоль за допомогою оператора typeof, наприклад
 let name = "Serhii";
 console.log(typeof name); в результаті в консолі ми побачимо - string

4. Поясніть чому '1' + 1 = 11.
В результаті коли ми за допомогою оператора + додаємо рядок "1" та число 1, відбувається конкатенація числа та рядка тобто злиття, і в результаті ми 
маємо рядок "11" */

// ПРАКТИЧНІ ПИТАННЯ

'use strict'

// 1. Оголосіть змінну і присвойте в неї число. Перевірте, чи ця змінна має тип "number" і виведіть результат в консоль.

let num = 1;
console.log(typeof num);

/* 2. Оголосіть змінну name і присвойте їй ваше ім'я. Оголосіть змінну lastName і присвойте в неї Ваше прізвище.
Виведіть повідомлення у консоль у форматі `Мене звати {ім'я}, {прізвище}` використовуючи ці змінні. */

let name = 'Serhii';
let lastName = 'Poslovskyi';
console.log(`Мене звати ${name} ${lastName}`);

// 3. Оголосіть змінну з числовим значенням і виведіть в консоль її значення всередині рядка.

let age = 37;
console.log(`Мені виповнилось ${age} років`);